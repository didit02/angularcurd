<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: access");
	header("Access-Control-Allow-Methods: GET");
	header("Access-Control-Allow-Credentials: true");
	header('Content-Type: application/json');
	 
	// include database adan nd object files
	include_once '../config/database.php';
	include_once '../objects/product.php';
	 
	// get database connection
	$database = new Database();
	$db = $database->getConnection();
	 
	// siapin product object
	$product = new Product($db);
	 
	// set ID property product yang akan diedit
	$product->id = isset($_GET['id']) ? $_GET['id'] : die();
	 
	// baca detail dari product yang akan diedit
	$product->readOne();
	 
	// buat array nya
	$product_arr = array(
	    "id" =>  $product->id,
	    "name" => $product->name,
	    "description" => $product->description,
	    "price" => $product->price,
	    "category_id" => $product->category_id,
	    "category_name" => $product->category_name
	 
	);
	 
	// make it json format
	print_r(json_encode($product_arr));
?>