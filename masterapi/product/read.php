<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/product.php';
 
// instantiate database dan product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$product = new Product($db);
 
// query products
$stmt = $product->read();
$num = $stmt->rowCount();
 
// check  jika > 0 record ditemukan
if($num>0){
    // products array
    $products_arr=array();
    $products_arr["records"]=array();
    // ambil konten
    // dengan fetch() 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract baris
        // gunakan  $row['name'] untuk
        // yang $name saja
        extract($row);
 
        $product_item=array(
            "id" => $id,
            "name" => $name,
            "description" => html_entity_decode($description),
            "price" => $price,
            "category_id" => $category_id,
            "category_name" => $category_name
        );
 
        array_push($products_arr["records"], $product_item);
    }
 
    echo json_encode($products_arr);
}
 
else{
    echo json_encode(
        array("message" => "No products found.")
    );
}
?>