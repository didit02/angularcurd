<?php
	// required headers
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	 
	// include database dan object files
	include_once '../config/database.php';
	include_once '../objects/product.php';
	 
	// get database connection
	$database = new Database();
	$db = $database->getConnection();
	 
	// prepare product object
	$product = new Product($db);
	 
	// ambil ID yang akan diedit
	$data = json_decode(file_get_contents("php://input"));
	 
	// set ID property produk to be edited
	$product->id = $data->id;
	 
	// set product property values
	$product->name = $data->name;
	$product->price = $data->price;
	$product->description = $data->description;
	$product->category_id = $data->category_id;
	 
	// update product
	if($product->update()){
	    echo '{';
	        echo '"message": "Product sukses update."';
	    echo '}';
	}
	 
	//jika gagal update 
	else{
	    echo '{';
	        echo '"message": "gagal update produk."';
	    echo '}';
	}
?>