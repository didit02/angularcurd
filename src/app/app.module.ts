import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";

import { HttpModule } from '@angular/http';
import { ReadProductsComponent } from './read-products/read-products.component';
import {ProductService} from "./product.service";
import { CreateProductComponent } from './create-product/create-product.component';
import { HomeComponent } from './home/home.component';
import { DeleteProductsComponent } from './delete-products/delete-products.component';
import { UpdateProductComponent } from './update-product/update-product.component';

@NgModule({
  declarations: [
    AppComponent,
    ReadProductsComponent,
    CreateProductComponent,
    HomeComponent,
    DeleteProductsComponent,
    UpdateProductComponent,

  ],
  imports: [
      BrowserModule,
      AppRoutingModule,
      HttpModule
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
