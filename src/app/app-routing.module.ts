import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';


import {HomeComponent} from "./home/home.component";
import {ReadProductsComponent} from "./read-products/read-products.component";
import {CreateProductComponent} from "./create-product/create-product.component";
import {DeleteProductsComponent} from "./delete-products/delete-products.component";
import {UpdateProductComponent} from "./update-product/update-product.component";


const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    {path: "home", component: HomeComponent},
    {path: "read/:some-data", component: ReadProductsComponent},
    {path: "create", component: CreateProductComponent},
    {path: "delete/:some-data", component: DeleteProductsComponent},
    {path: "update/:some-data", component: UpdateProductComponent},
    {path: "create", component: CreateProductComponent}
    ];



@NgModule({
    imports: [RouterModule.forRoot(routes)], exports: [RouterModule]
})
export class AppRoutingModule {
}

