import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {ProductService} from "../product.service";


@Component({
    selector: 'app-delete-products',
    templateUrl: './delete-products.component.html',
    styleUrls: ['./delete-products.component.css']
})
export class DeleteProductsComponent implements OnInit {
    public subs;
    public params;
    public dataresult;

    constructor(public router: Router, private route: ActivatedRoute, public service: ProductService) {
    }

    ngOnInit() {
        this.subs = this.route.params.subscribe(params => {
            this.params = params['some-data'];
        });
    }

    remove (){
        this.service.deleteOneProducts(this.params).subscribe(data => {
                this.router.navigate(['/home']);
        })
    }

}
