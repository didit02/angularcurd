import {Component, OnInit} from '@angular/core';
import {ProductService} from "../product.service";
import {Router} from "@angular/router";



@Component({
    selector: 'app-home', templateUrl: './home.component.html', styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    public products = [];

    constructor(private service: ProductService,public router: Router) {
    }

    ngOnInit() {
        this.service.readProducts().subscribe(data => {
            let dataresult = data['records'];
            for (var i = 0; i < dataresult.length; i++) {
                this.products.push(dataresult[i])
            }
        })
    }
    clickId(id){
        this.router.navigate(['/read', id]);
    }

    remove(id){
        this.router.navigate(['/delete',id]);
    }
    update(id){
        this.router.navigate(['/update',id]);
    }

}
