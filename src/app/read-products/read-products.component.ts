import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from "../product.service";

@Component({
    selector: 'app-read-products',
    templateUrl: './read-products.component.html',
    styleUrls: ['./read-products.component.css']
})
export class ReadProductsComponent implements OnInit {
    public subs;
    public params;
    public dataresult;

    constructor(public router: Router, private route: ActivatedRoute, public service: ProductService) {
    }

    ngOnInit() {
        this.subs = this.route.params.subscribe(params => {
            this.params = params['some-data'];
        });


        this.service.readOneProducts(this.params).subscribe(data => {
            this.dataresult = data;
        })
    }

}
